var http = require('http'); // Permet de créer un server
var url = require('url'); // Permet de parser l'url pour redirection
var querystring = require('querystring'); // Permet de parser les différents paramètres passé en GET

var server = http.createServer(function(req, res) { // Function qui créer un server

    var page = url.parse(req.url).pathname; // Découpe l'url
    var params = querystring.parse(url.parse(req.url).query); // Découpe les paramètres contenu dans l'url

    console.log(page); // Affiche dans la console la variable 'page' parsé précédement 

    res.writeHead(200, {"Content-Type": "text/plain"}); // Renvoi le code http 200

    // Permet de faire une redirection en fonction de l'url
    if (page == '/') {
        res.write('Vous êtes à l\'accueil, que puis-je pour vous ');

        // Permet d'afficher les paramètres contenu dans l'url
	    if ('prenom' in params && 'nom' in params) {
	        res.write(params['prenom'] + ' ' + params['nom']);
	    } else {
	        res.write('visiteur');
	    }

	    res.write(' ?');

    } else if (page == '/sous-sol') {
        res.write('Vous êtes dans la cave à vins, ces bouteilles sont à moi !');
    } else if (page == '/private') {
        res.write('Hé ho, c\'est privé ici !');
    } else {
    	res.write('404 not found !!');
    }


  	



    res.end();
});
server.listen(8080);