var express = require('express'); // Appel express

var app = express(); // Appel la function express

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.send('Vous êtes à l\'accueil');
});

app.get('/', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.send('Vous êtes à l\'accueil, que puis-je pour vous ?');
});

app.get('/soussol', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.send('Vous êtes dans la cave à vins, ces bouteilles sont à moi !');
});

app.get('/private', function(req, res) {
    res.setHeader('Content-Type', 'text/plain');
    res.send('Hé ho, c\'est privé ici !');
});

app.get('/etage/:etageNum/chambre/:roomNum', function(req, res) { // Avec express, posibilité de faire passer des var dynamique :etageNum
    res.setHeader('Content-Type', 'text/plain');
    // res.end('Vous êtes à la chambre ' + req.params.roomNum + ' de l\'étage n°' + req.params.etageNum); // Ici on affiche la var dynamique
    
    var names = ['Arnaud', 'Julie', 'Zoé'];
    var count = Math.round(Math.random() * 100);
    // Avec le templates ejs (npm i ejs)
    res.render('chambre.ejs', {etage: req.params.etageNum, room: req.params.roomNum, noms: names, compteur: count});
    // On appel le fichier de rendu qui ce trouve dans un dossier views et on lui passe les paramètres à modifier
});

app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page introuvable !');
});

app.listen(8080);